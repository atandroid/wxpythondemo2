# -*- coding: utf-8 -*-
__author__ = 'sht'
__date__ = '2024-05-18 17:40'

import wx


class Tab3(wx.Panel):
    def __init__(self, parent):
        super().__init__(parent)

        sizer = wx.BoxSizer(wx.VERTICAL)

        btn = wx.Button(self, label="Button in Tab 3")
        btn.Bind(wx.EVT_BUTTON, self.on_button_click)

        sizer.Add(btn, 0, wx.ALL | wx.CENTER, 10)
        self.SetSizer(sizer)

    def on_button_click(self, event):
        wx.MessageBox("Button in Tab 3 clicked!", "Info", wx.OK | wx.ICON_INFORMATION)
