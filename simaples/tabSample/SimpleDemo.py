# -*- coding: utf-8 -*-
__author__ = 'sht'
__date__ = '2024-05-18 17:40'


import wx
class MyFrame(wx.Frame):
    def __init__(self):
        super().__init__(None, title="Hello wxPython", size=(300, 200))
        panel = wx.Panel(self)
        btn = wx.Button(panel, label="Click Me!", pos=(100, 50))
        btn.Bind(wx.EVT_BUTTON, self.on_button_click)

    def on_button_click(self, event):
        wx.MessageBox("Button clicked!", "Info", wx.OK | wx.ICON_INFORMATION)


def start():
    app = wx.App(False)
    frame = MyFrame()
    frame.Show()
    app.MainLoop()


