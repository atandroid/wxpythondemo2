# -*- coding: utf-8 -*-
__author__ = 'sht'
__date__ = '2024-05-18 17:40'

import wx

from simaples.tabSample.tabs.Tab1 import Tab1
from simaples.tabSample.tabs.Tab2 import Tab2
from simaples.tabSample.tabs.Tab3 import Tab3


class MainFrame(wx.Frame):
    def __init__(self):
        super().__init__(None, title="Tab切换例子", size=(800, 600))
        panel = wx.Panel(self)

        notebook = wx.Notebook(panel)

        tab1 = Tab1(notebook)
        tab2 = Tab2(notebook)
        tab3 = Tab3(notebook)

        notebook.AddPage(tab1, "Tab 1")
        notebook.AddPage(tab2, "Tab 2")
        notebook.AddPage(tab3, "Tab 3")

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(notebook, 1, wx.EXPAND)
        panel.SetSizer(sizer)

        self.Centre()
        self.Show()


class MainApp(wx.App):
    def OnInit(self):
        self.frame = MainFrame()
        self.frame.Show()
        return True


def start():
    app = MainApp()
    app.MainLoop()

if __name__ == '__main__':
    app = MainApp()
    app.MainLoop()