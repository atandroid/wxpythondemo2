import wx
from simaples import BrowserDemo
from simaples.tabSample import TabHomeView, SimpleDemo


class MainFrame(wx.Frame):
    def __init__(self):
        super().__init__(None, title="Main Window", size=(400, 300))
        panel = wx.Panel(self)

        vbox = wx.BoxSizer(wx.VERTICAL)

        btn2 = wx.Button(panel, label="打开simple例子")
        btn2.Bind(wx.EVT_BUTTON, self.on_open_simple_demo)

        btn1 = wx.Button(panel, label="打开浏览器")
        btn1.Bind(wx.EVT_BUTTON, self.on_open_browser_demo)

        btn4 = wx.Button(panel, label="tab切换")
        btn4.Bind(wx.EVT_BUTTON, self.on_tab_change)

        vbox.Add(btn2, 0, wx.ALL | wx.CENTER, 5)
        vbox.Add(btn1, 0, wx.ALL | wx.CENTER, 5)
        vbox.Add(btn4, 0, wx.ALL | wx.CENTER, 5)

        panel.SetSizer(vbox)

    def on_open_browser_demo(self, event):
        BrowserDemo.start()

    def on_open_simple_demo(self, event):
        SimpleDemo.start()
    def on_tab_change(self,event):
        TabHomeView.start()

class MainApp(wx.App):
    def OnInit(self):
        self.frame = MainFrame()
        self.frame.Show()
        return True

def start():
    app = MainApp()
    app.MainLoop()